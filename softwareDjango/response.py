from rest_framework.status import (HTTP_200_OK, HTTP_400_BAD_REQUEST,
                                   HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND,
                                   HTTP_500_INTERNAL_SERVER_ERROR)
from rest_framework.response import Response


class BaseResponse(Response):
    def __init__(self, code: int, message: str, data: object, status: int,
                 template_name=None, exception=False, content_type='application/json'):
        super(Response, self).__init__(None, status=status)
        self._code = code
        self._message = message
        self._data = data
        self._status = status
        self.data = {"code": code, "message": message, "data": data}
        self.template_name = template_name
        self.exception = exception
        self.content_type = content_type

    @staticmethod
    def SUCCESS(data=None, message="ok", code=20000, status=HTTP_200_OK):
        return BaseResponse(code=code, message=message, data=data, status=status)

    @staticmethod
    def ERROR(code=50000, message="发生未知错误", status=HTTP_200_OK, data=None):
        return BaseResponse(code=code, message=message, data=data, status=status)

    @property
    def code(self):
        return self._code

    @code.setter
    def code(self, value):
        self._code = value

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, value):
        self._message = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def status(self):
        return self._status

    @status.setter
    def code(self, value):
        self._status = value


class Status:
    OK = BaseResponse.SUCCESS()
    ERROR = BaseResponse.ERROR()
    MISSING_PARAMETER = BaseResponse.ERROR(50001, '请求参数不完整', HTTP_400_BAD_REQUEST)
    USER_ALREADY_EXIT = BaseResponse.ERROR(50002, "用户名已存在", HTTP_400_BAD_REQUEST)
    ERROR_INPUT = BaseResponse.ERROR(50003, '输入数据有误', HTTP_400_BAD_REQUEST)
    IMAGES_ERROR = BaseResponse.ERROR(50004, '图片大小或数量不符要求', HTTP_400_BAD_REQUEST)
    INTERNAL_SERVER_ERROR = BaseResponse.ERROR(
        50005, '服务器内部错误', HTTP_500_INTERNAL_SERVER_ERROR)
    USER_NOT_EXIST = BaseResponse.ERROR(50006, '当前用户不存在', HTTP_400_BAD_REQUEST)
    RECORD_NOT_FOUND = BaseResponse.ERROR(50007, '查找的数据不存在', HTTP_400_BAD_REQUEST)
    NETWORK_NOT_CONNECT = BaseResponse.ERROR(50008, '网络异常', HTTP_404_NOT_FOUND)
    PERMISSION_DENIED = BaseResponse.ERROR(50009, '当前用户没有操作权限', HTTP_403_FORBIDDEN)
    NEED_LOG_IN = BaseResponse.ERROR(50010, '需要重新登录，获取token', HTTP_403_FORBIDDEN)
