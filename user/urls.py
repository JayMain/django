from django.urls import path
from .views import UserLogin, UserRegister, UserGetInfo

urlpatterns = [
    # get
    # path('<int:user_id>/', OtherUserInformation.as_view()),
    path('login', UserLogin.as_view()),
    path('register', UserRegister.as_view()),
    path('getinfo', UserGetInfo.as_view()),

]
