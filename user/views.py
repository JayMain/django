from rest_framework.views import APIView

from softwareDjango.response import BaseResponse, Status
from user import services
from user.auth import AuthView
from user.serializers import UserSerializer


class UserLogin(APIView):
    def post(self, request):
        try:
            return services.login(request.data['username'], request.data['password'])
        except:
            return Status.MISSING_PARAMETER


class UserRegister(APIView):
    def post(self, request):
        try:
            return services.register(request.data['username'], request.data['password'])
        except:
            return Status.MISSING_PARAMETER


class UserGetInfo(AuthView):
    def post(self, request):
        try:
            return BaseResponse.SUCCESS(data=UserSerializer(request.user).data)
        except:
            return Status.USER_NOT_EXIST
