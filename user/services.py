from softwareDjango.response import BaseResponse, Status
from user.models import User


def login(username: str, password: str):
    try:
        user = User.objects.get(username=username, password=password)
        response_data = BaseResponse.SUCCESS(data={
            'token': user.accountId
        })
    except:
        response_data = BaseResponse.ERROR(message="用户名或者密码错误")
    return response_data


def register(username: str, password: str):
    try:
        user = User(username=username, password=password)
        user.save()
        response_data = Status.OK
    except:
        response_data = Status.USER_ALREADY_EXIT
    return response_data
