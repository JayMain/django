from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.request import Request
from rest_framework.views import APIView

from .models import User

one_day = 60 * 60 * 24


class Authentication(BaseAuthentication):
    def authenticate(self, request: Request):
        token = request.META.get('HTTP_AUTHORIZATION')
        if not token:
            raise AuthenticationFailed('未携带token', 403)
        try:
            if token.index('Bearer ') >= 0:
                id = token.split(' ')[1].strip()
        except:
            raise AuthenticationFailed('token解析错误', 403)
        try:
            user = User.objects.get(accountId=id)
            return user, None
        except User.DoesNotExist:
            raise AuthenticationFailed('用户不存在', 401)


class AuthView(APIView):
    authentication_classes = [Authentication]
