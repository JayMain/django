from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models


class User(AbstractBaseUser):
    class Role(models.TextChoices):
        USER = 'user'
        ADMIN = 'admin'

    accountId = models.BigAutoField(primary_key=True)
    username = models.CharField(max_length=64, null=True, blank=True, unique=True)
    name = models.CharField(max_length=64, null=True, blank=True, default='张三')
    avatar = models.CharField(max_length=512, null=True, blank=True,
                              default='https://s.cn.bing.net/th?id=OIP-C.mJbeu6FCJuoNLGnS-n-2cwAAAA&w=249&h=250&c=8'
                                      '&rs=1&qlt=90&o=6&pid=3.1&rm=2')
    email = models.CharField(max_length=64, null=True, blank=True, default='zhangsan@163.com')
    job = models.CharField(max_length=32, null=True, blank=True, default='fronted')
    jobName = models.CharField(max_length=64, null=True, blank=True, default='前端艺术家')
    organization = models.CharField(max_length=64, null=True, blank=True, default='kylin')
    organizationName = models.CharField(max_length=64, null=True, blank=True, default='麒麟')
    location = models.CharField(max_length=64, null=True, blank=True, default='beijing')
    locationName = models.CharField(max_length=64, null=True, blank=True, default='北京')
    introduction = models.CharField(
        max_length=512, null=True, blank=True, default='')
    personalWebsite = models.CharField(max_length=128, null=True, blank=True, default='')
    phone = models.CharField(max_length=11, null=True, blank=True, default='155********')
    certification = models.IntegerField(null=True, blank=True, default=1)
    role = models.CharField(
        max_length=8, default=Role.USER, choices=Role.choices, blank=True)
    password = models.CharField(max_length=128, null=True, blank=True)
    USERNAME_FIELD = 'username'
