from influxdb_metrics.utils import query, write_points
import time


class InfluxdbUtils(object):
    @staticmethod
    def time_query(hostIp: str, virtual_name: str, time_gap: int):
        try:
            begin_time = "{:.0f}".format(time.time_ns() - time_gap * 6e10)
            res = query(
                f"select time,cpu_usage,disk_space_usage,memory_usage,network_usage from usage where host='{hostIp}' "
                f"and virtual_name='{virtual_name}' and time>={begin_time}  order by time desc")
            return list(res.get_points())
        except Exception as e:
            print(e.__str__())
            raise Exception("influxdb查询失败")

    @staticmethod
    def query_usage(label: str, hostIp: str, virtual_name: str, counts: int):
        try:
            res = query(f"select {label} from usage where host='{hostIp}' and "
                        f"virtual_name='{virtual_name}' order by time desc limit {counts}")
            return list(res.get_points())
        except:
            raise Exception("influxdb查询失败")

    @staticmethod
    def query_all(hostIp: str, virtual_name: str, counts: int):
        try:
            res = query(
                f"select time,cpu_usage,disk_space_usage,memory_usage,network_usage from usage where host='{hostIp}' "
                f"and virtual_name='{virtual_name}' order by time desc limit {counts}")
            return list(res.get_points())
        except:
            raise Exception("influxdb查询失败")

    @staticmethod
    def is_monitor_state(hostIp: str, virtual_name: str):
        try:
            res = query(f"select * from usage where host='{hostIp}' and "
                        f"virtual_name='{virtual_name}' limit 1")
            return len(res) > 0
        except:
            raise Exception("influxdb查询失败")

    @staticmethod
    def insert_usage(cpu_usage: float, disk_space_usage: float, memory_usage: float,
                     network_usage: float, hostIp: str, virtual_name: str):
        try:
            json_body = [{
                "measurement": "usage",
                "tags": {
                    "host": hostIp,
                    "virtual_name": virtual_name
                },
                "fields": {
                    "memory_usage": memory_usage,
                    "cpu_usage": cpu_usage,
                    "disk_space_usage": disk_space_usage,
                    "network_usage": float(network_usage),
                }
            }]
            write_points(json_body)
        except:
            raise Exception("influxdb插入失败")
