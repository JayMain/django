from rest_framework.serializers import ModelSerializer
from .models import *


class VirtualSerializer(ModelSerializer):
    class Meta:
        model = Virtual
        fields = ('Id', 'virtualName')


class ServerSerializer(ModelSerializer):
    virtual_machines = VirtualSerializer(many=True)

    class Meta:
        model = Server
        fields = ('Id', 'hostIp', 'virtual_machines')
