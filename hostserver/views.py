from django.shortcuts import render
from user.auth import AuthView
from .service import *
from softwareDjango.response import BaseResponse
from .models import *


class UserAddNew(AuthView):
    def post(self, request):
        try:
            body = request.data
            if 'virtualName' in body.keys():
                return connect_new_virtual(user=request.user, hostIp=body['hostIp'],
                                           virtualName=body['virtualName'])
            else:
                port = 22
                if 'port' in body.keys():
                    port = body['port']
                return connect_new_host(host_ip=body['hostIp'], username=body['username'],
                                        password=body['password'], user=request.user, port=port)
        except:
            return Status.MISSING_PARAMETER


class UserRemove(AuthView):
    def post(self, request):
        try:
            body = request.data
            if 'hostId' in body.keys():
                return remove_host(request.data['hostId'])
            else:
                return remove_virtual(request.data['virtualId'])
        except:
            return Status.MISSING_PARAMETER


class ExecCommand(AuthView):
    def post(self, request):
        try:
            if 'hostIp' in request.data.keys():
                return exec_one_host(hostIp=request.data['hostIp'], command=request.data['command'], user=request.user)
            else:
                return exec_all_host(request.data['command'], user=request.user)
        except:
            return Status.MISSING_PARAMETER


class showView(AuthView):
    def post(self, request):
        try:
            if 'hostIp' in request.data.keys():
                return show_all_virtual_machine(hostIp=request.data['hostIp'], user=request.user)
            else:
                return get_all_hosts(user=request.user)
        except:
            return Status.MISSING_PARAMETER


class monitorView(AuthView):
    def post(self, request):
        try:
            return monitor(request.data['virtualId'])
        except:
            return Status.MISSING_PARAMETER


class queryView(AuthView):
    def post(self, request):
        try:
            counts = 5
            body = request.data
            if 'time' in body.keys():
                return time_query_usage(body['virtualId'], body['time'])
            label = None
            if 'counts' in body.keys():
                counts = body['counts']
            if 'label' in body.keys():
                label = body['label']
            return query_usage(label, body['virtualId'], counts)
        except:
            return Status.MISSING_PARAMETER


class GetInfoView(AuthView):
    def post(self, request):
        try:
            return get_virtual_info(request.data['virtualId'])
        except:
            return Status.MISSING_PARAMETER
