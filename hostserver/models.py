from django.db import models
from user.models import User


class Server(models.Model):
    Id = models.BigAutoField(primary_key=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='servers', related_query_name='server')
    hostIp = models.CharField(max_length=64, null=True, blank=True)
    port = models.IntegerField(null=True, blank=True, default=22)
    username = models.CharField(max_length=64, null=True, blank=True)
    password = models.CharField(max_length=64, null=True, blank=True)

    class Meta:
        unique_together = (
            ('owner', 'hostIp'),  # 联合唯一
        )


class Virtual(models.Model):
    Id = models.BigAutoField(primary_key=True)
    server = models.ForeignKey(Server, on_delete=models.CASCADE, related_name='virtual_machines',
                               related_query_name='virtual_machine')
    virtualName = models.CharField(max_length=64, null=True, blank=True)

    class Meta:
        unique_together = (
            ('server', 'virtualName'),  # 联合唯一
        )
