from .client import SshClient
from .models import *
from softwareDjango.response import Status
from user.models import User
from softwareDjango.response import BaseResponse
from .influxdb import InfluxdbUtils
from .serializers import *
from .libvirt import LibvirtUtils


def connect_new_host(host_ip: str, username: str, password: str, user: User, port: int):
    try:
        if SshClient.is_success_connect(host_ip=host_ip,
                                        username=username, password=password, port=port):
            server = Server(owner=user, hostIp=host_ip,
                            username=username, password=password, port=port)
            server.save()
            user.servers.add(server)
            return BaseResponse.SUCCESS(data=ServerSerializer(server).data)
        else:
            return BaseResponse.ERROR(message="连接失败，用户或密码错误")
    except Exception as e:
        print(e)
        return BaseResponse.ERROR(message="用户和主机已绑定")


def connect_new_virtual(user: User, hostIp: str, virtualName: str):
    try:
        server = Server.objects.get(hostIp=hostIp, owner=user)
        virtual = Virtual(server=server, virtualName=virtualName)
        virtual.save()
        server.virtual_machines.add(virtual)
        return BaseResponse.SUCCESS(data=VirtualSerializer(virtual).data)
    except Exception as e:
        print(e.__str__())
        return BaseResponse.ERROR(message="服务器错误或者用户与虚拟机已绑定")


def remove_virtual(virtual_id: int):
    try:
        virtual = Virtual.objects.get(Id=virtual_id)
        virtual.delete()
    except Exception as e:
        return BaseResponse.ERROR(message="用户与虚拟机未绑定")
    return Status.OK


def remove_host(id: int):
    try:
        server = Server.objects.get(Id=id)
        virtuals = Virtual.objects.filter(server=server)
        virtuals.delete()
        server.delete()
    except Exception as e:
        print(e)
        return BaseResponse.ERROR(message="用户与主机未绑定")
    return Status.OK


def get_all_hosts(user: User):
    try:
        servers = user.servers
        return BaseResponse.SUCCESS(data=ServerSerializer(servers, many=True).data)
    except Exception as e:
        print(e.__str__())
        return Status.ERROR


def exec_all_host(command: str, user: User):
    try:
        user_client = SshClient(user=user)
        result = user_client.exec_all(command=command)
    except:
        return BaseResponse.ERROR(message="服务器连接发生错误")
    return BaseResponse.SUCCESS(data=result)


def exec_one_host(hostIp: str, command: str, user: User):
    try:
        user_client = SshClient(user=user)
        result = user_client.exec_one_host(hostIp=hostIp, command=command)
    except:
        return BaseResponse.ERROR(message="服务器连接发生错误")
    return BaseResponse.SUCCESS(data=result)


def show_all_virtual_machine(hostIp: str, user: User):
    try:
        server = Server.objects.get(hostIp=hostIp, owner=user)
        return BaseResponse.SUCCESS(data=LibvirtUtils.show_all_virtual_name(server=server))
    except Exception as e:
        return BaseResponse.ERROR(message=e.__str__())


def query_usage(label: str, virtual_Id: int, counts: int):
    try:
        virtual = Virtual.objects.get(Id=virtual_Id)
        if label is None:
            res = InfluxdbUtils.query_all(virtual.server.hostIp, virtual.virtualName, counts)
        else:
            res = InfluxdbUtils.query_usage(label, virtual.server.hostIp, virtual.virtualName, counts)
        return BaseResponse.SUCCESS(data=res)
    except Exception as e:
        return BaseResponse.ERROR(message=e.__str__())


def time_query_usage(virtual_Id: int, time: int):
    try:
        virtual = Virtual.objects.get(Id=virtual_Id)
        res = InfluxdbUtils.time_query(virtual.server.hostIp, virtual.virtualName, time)
        return BaseResponse.SUCCESS(data=res)
    except Exception as e:
        return BaseResponse.ERROR(message=e.__str__())


def get_virtual_info(virtualId: int):
    try:
        virtual = Virtual.objects.get(Id=virtualId)
        res = LibvirtUtils.get_virtual_info(virtual)
        return BaseResponse.SUCCESS(data=res)
    except Exception as e:
        return BaseResponse.ERROR(message=e.__str__())


def monitor(virtualId: int):
    try:
        virtual = Virtual.objects.get(Id=virtualId)
        LibvirtUtils.start_monitor_virtual(virtual)
        return Status.OK
    except Exception as e:
        print(e.__str__())
        return BaseResponse.ERROR(message=e.__str__())
