import os, django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "softwareDjango.settings")
django.setup()
import time

import libvirt
from .models import *
from multiprocessing import Process
from .influxdb import InfluxdbUtils
from xml.etree import ElementTree


class LibvirtUtils(object):
    @staticmethod
    def show_all_virtual_name(server: Server):
        conn = None
        result = []
        try:
            uri = 'qemu+ssh://{username}@{hostIp}:{port}/system?socket=/var/run/libvirt/libvirt-sock'.format(
                username=server.username, hostIp=server.hostIp, port=server.port)
            conn = libvirt.open(uri)
            for id in conn.listDomainsID():
                domain = conn.lookupByID(id)
                result.append(domain.name())
            return result
        except:
            raise Exception("服务器libvirt连接错误")
        finally:
            if conn is not None:
                conn.close()

    @staticmethod
    def start_monitor_virtual(virtual: Virtual):
        if InfluxdbUtils.is_monitor_state(hostIp=virtual.server.hostIp, virtual_name=virtual.virtualName):
            return
        p = MonitorProcess(server=virtual.server, virtual_name=virtual.virtualName)
        p.start()

    @staticmethod
    def get_virtual_info(virtual: Virtual):
        conn = None
        result = {}
        try:
            uri = 'qemu+ssh://{username}@{hostIp}:{port}/system?socket=/var/run/libvirt/libvirt-sock'.format(
                username=virtual.server.username, hostIp=virtual.server.hostIp, port=virtual.server.port)
            conn = libvirt.open(uri)
            domain = conn.lookupByName(virtual.virtualName)
            result['cpu_counts'] = domain.info()[3]
            result['state'] = domain.info()[0]
            result['max_memory'] = domain.info()[1]
            result['used_memory'] = domain.info()[2]
            meminfo = domain.memoryStats()
            result['available'] = meminfo['available']
            result['actual'] = meminfo['actual']
            result['unused'] = meminfo['unused']
            result['rss'] = meminfo['rss']
            return result
        except:
            raise Exception("服务器libvirt连接错误")
        finally:
            if conn is not None:
                conn.close()


class MonitorProcess(Process):  # 继承Process类
    def __init__(self, server: Server, virtual_name: str):
        super(MonitorProcess, self).__init__()
        self.server = server
        self.virtual_name = virtual_name

    def run(self):
        global network_usage, disk_space_usage
        conn = None
        print("begin monitor")
        try:
            uri = 'qemu+ssh://{username}@{hostIp}:{port}/system?socket=/var/run/libvirt/libvirt-sock'.format(
                username=self.server.username, hostIp=self.server.hostIp, port=self.server.port)
            conn = libvirt.open(uri)
            domain = conn.lookupByName(self.virtual_name)
            while True:
                # 开始计算cpu使用率
                t1 = time.time()
                c1 = float(domain.info()[4])
                time.sleep(5)
                t2 = time.time()
                c2 = float(domain.info()[4])
                c_nums = float(domain.info()[3])
                cpu_usage = (c2 - c1) * 100 / ((t2 - t1) * c_nums * 1e9)

                # 开始计算虚拟机内存
                domain.setMemoryStatsPeriod(10)
                meminfo = domain.memoryStats()
                free_mem = float(meminfo['unused'])
                total_mem = float(meminfo['available'])
                memory_usage = ((total_mem - free_mem) / total_mem) * 100

                # 获取网络流量
                tree = ElementTree.fromstring(domain.XMLDesc())
                ifaces = tree.findall('devices/interface/target')
                for i in ifaces:
                    iface = i.get('dev')
                    network_usage = domain.interfaceStats(iface)[0]
                    break

                # 获取磁盘信息
                devices = tree.findall('devices/disk/target')
                for d in devices:
                    try:
                        device = d.get('dev')
                        devinfo = domain.blockInfo(device)
                        disk_space_usage = float(devinfo[1]) / float(devinfo[0]) * 100
                        break
                    except libvirt.libvirtError:
                        pass
                InfluxdbUtils.insert_usage(cpu_usage, disk_space_usage, memory_usage, network_usage,
                                           self.server.hostIp, self.virtual_name)
        except Exception as e:
            print(e.__str__())
        finally:
            print("done")
            if conn is not None:
                conn.close()
