# 导入ssh client模块
from paramiko.client import SSHClient, AutoAddPolicy
# 导入异常处理
from user.models import User


# 创建SshClient 类
class SshClient():
    # 初始化
    def __init__(self, user: User):
        self.user = user
        self.server_clients = {}

    def __connect_all__(self):
        servers = self.user.servers.all()
        for server in servers:
            self.server_clients[server.hostIp] = self.__connect__(host_ip=server.hostIp, username=server.username
                                                                  , password=server.password, port=server.port)

    def __connect_one__(self, hostIp: str):
        server = self.user.servers.get(hostIp=hostIp)
        self.server_clients[hostIp] = self.__connect__(host_ip=server.hostIp, username=server.username
                                                       , password=server.password, port=server.port)

    def __close_all__(self):
        keys = tuple(self.server_clients.keys())
        for hostIp in keys:
            self.server_clients[hostIp].close()
            del self.server_clients[hostIp]

    def exec_all(self, command: str):
        self.__connect_all__()
        result_list = {}
        for hostIp, client in self.server_clients.items():
            result_list[hostIp] = self.__execute_some_command__(client, command)
        self.__close_all__()
        return result_list

    def exec_one_host(self, hostIp: str, command: str):
        self.__connect_one__(hostIp)
        result = self.__execute_some_command__(self.server_clients[hostIp], command)
        self.server_clients[hostIp].close()
        del self.server_clients[hostIp]
        return result

    @staticmethod
    def is_success_connect(host_ip: str, username: str, password: str, port=22):
        client = SshClient.__connect__(host_ip, username, password, port)
        if client:
            client.close()
            return True
        else:
            return False

    @staticmethod
    def __connect__(host_ip: str, username: str, password: str, port=22):
        ssh_client = SSHClient()
        try:
            # 允许连接不在known_hosts文件中的主机，及自动输入yes
            ssh_client.set_missing_host_key_policy(AutoAddPolicy())
            ssh_client.connect(host_ip, port=port, username=username, password=password)
        except:
            return None
        return ssh_client

    @staticmethod
    def __execute_some_command__(ssh_client: SSHClient, command: str):
        if ssh_client:
            stdin, stdout, stderr = ssh_client.exec_command(command, get_pty=True)
            res, err = stdout.read(), stderr.read()
            result = res if res else err
            res = result.decode(encoding="utf-8")
            return res
