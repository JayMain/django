from django.urls import path
from .views import *

urlpatterns = [
    path('add', UserAddNew.as_view()),
    path('exec', ExecCommand.as_view()),
    path('remove', UserRemove.as_view()),
    path('show', showView.as_view()),
    path('query', queryView.as_view()),
    path('monitor', monitorView.as_view()),
    path('get_info', GetInfoView.as_view()),
]
